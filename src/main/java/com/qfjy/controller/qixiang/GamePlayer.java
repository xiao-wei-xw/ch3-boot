package com.qfjy.controller.qixiang;

public class GamePlayer implements IGamePlayer{
    String name;

    public GamePlayer(String name) {
        this.name = name;
    }

    @Override
    public void login() {
        System.out.println(name+"已登陆游戏！");
    }

    @Override
    public void killBoss() {
        System.out.println(name+"正在刷怪！");
    }

    @Override
    public void upgrade() {
        System.out.println("恭喜升级！");
    }
}
