package com.qfjy.controller.qixiang;

public interface IGamePlayer {
    /*
     * 登陆游戏
     */
    public void login();

    /*
     * 杀怪
     */
    public void killBoss();

    /*
     * 升级
     */
    public void upgrade();
}
