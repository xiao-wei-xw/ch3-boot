package com.qfjy.controller.qixiang;

public class GamePlayProxy implements IGamePlayer{
    IGamePlayer gamePlay;

    /*
     * 构造函数
     */
    public GamePlayProxy(IGamePlayer gamePlay) {
        this.gamePlay = gamePlay;
    }

    @Override
    public void login() {
        System.out.println("--->开始玩游戏前，教练指导战术！");
        this.gamePlay.login();
    }

    @Override
    public void killBoss() {
        this.gamePlay.killBoss();
    }

    @Override
    public void upgrade() {
        this.gamePlay.upgrade();
        System.out.println("--->游戏结束，教练总结经验！");
    }
}
