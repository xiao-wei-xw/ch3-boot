package com.qfjy.controller.qixiang;

public class ProxyClass {
    public static void main(String[] args) {
        GamePlayer gamePlay = new GamePlayer("张三");
        GamePlayProxy proxy = new GamePlayProxy(gamePlay);
        proxy.login();
        proxy.killBoss();
        proxy.upgrade();
    }
}

