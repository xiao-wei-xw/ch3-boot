package com.qfjy.java2102.xiaowei;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: 肖伟
 * @Date: 2021/08/05/0:03
 * @Description:
 */
public class CompositeEntity {
    private CoarseGrainedObject cgo = new CoarseGrainedObject();

    public void setData(String data1, String data2){
        cgo.setData(data1, data2);
    }

    public String[] getData(){
        return cgo.getData();
    }
}
