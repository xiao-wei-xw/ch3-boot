package com.qfjy.java2102.xiaowei;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: 肖伟
 * @Date: 2021/08/05/0:04
 * @Description:
 */
public class Client {
    private CompositeEntity compositeEntity = new CompositeEntity();

    public void printData(){
        for (int i = 0; i < compositeEntity.getData().length; i++) {
            System.out.println("Data: " + compositeEntity.getData()[i]);
        }
    }

    public void setData(String data1, String data2){
        compositeEntity.setData(data1, data2);
    }
}
