package com.qfjy.java2102.xiaowei;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: 肖伟
 * @Date: 2021/08/05/0:01
 * @Description:
 */
public class DependentObject1 {
    private String data;

    public void setData(String data){
        this.data = data;
    }

    public String getData(){
        return data;
    }
}
