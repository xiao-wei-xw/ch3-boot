package com.qfjy.java2102.xiaowei;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: 肖伟
 * @Date: 2021/08/05/0:04
 * @Description:
 */
public class CompositeEntityPatternDemo {
    public static void main(String[] args) {
        Client client = new Client();
        client.setData("Test", "Data");
        client.printData();
        client.setData("Second Test", "Data1");
        client.printData();
    }
}
