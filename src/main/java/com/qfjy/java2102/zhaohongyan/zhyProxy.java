package com.qfjy.java2102.zhaohongyan;
public class zhyProxy {
    /**
     * 代理模式
     */
    //接口
    public interface ProxyDemo {
        void show();
    }

    //    ProxyDemo的实现类，目标实现类
    public class ProxyTest implements ProxyDemo {
        @Override
        public void show() {
            System.out.println("目标实现类");
        }
    }

    //    代理类，实现ProxyDemo接口;
    public class ProxyStatic implements ProxyDemo {
        private ProxyTest proxyTest;
        public ProxyStatic(ProxyTest proxyTest) {
            this.proxyTest = proxyTest;
        }
        @Override
        public void show() {
            System.out.println("---开始--");
            proxyTest.show();
            System.out.println("--结束--");

        }
    }
}
