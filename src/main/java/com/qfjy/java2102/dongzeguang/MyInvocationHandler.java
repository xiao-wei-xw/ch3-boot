package com.qfjy.java2102.dongzeguang;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {
    private UserService us;
    public MyInvocationHandler(UserService us) {
        this.us = us;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        /*
           proxy 代理对象
           method 被代理对象里面的方法
           args   被代理对象里面的方法 被传入的参数
         */

        //method.invoke()  调用被代理对象的方法
        System.out.println("开启事务");
        //这个方法对应的对象
        Object o = method.invoke(us, args);
        System.out.println("提交事务");
        return o;
    }
}
