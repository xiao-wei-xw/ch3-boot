package com.qfjy.java2102.dongzeguang;

import java.lang.reflect.Proxy;

/**
    动态代理
    对象，做一些操作
    生成一个代理对象，代理你做一些事
    目的：对对象的一种增强
    动态代理缺点
        被代理对象必须要有接口
    Cglib代理
 */
public class ProxyDemo {
    public static void main(String[] args) {
        UserService us=new UserServiceImpl();
//        us.add();
//        us.delete();
//        us.select();
//        us.update();

        //动态代理
        /**
          要代理的对象  us   要增强的对象
           代理对象    superus  增强后的对象
         类加载器  对象.getClass().getClassLoader()
         要被代理的对象的所有实现的接口
         你要怎么增强   处理器
         newProxyInstance(ClassLoader loader,Class<?>[] interfaces,reflect.InvocationHandler h)
         */

        MyInvocationHandler mih=new MyInvocationHandler(us);


        UserService uss = (UserService) Proxy.newProxyInstance(us.getClass().getClassLoader(),
                us.getClass().getInterfaces(), mih);

//        us.add();
        uss.add();
    }
}
