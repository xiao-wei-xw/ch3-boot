package com.qfjy.java2102.dongzeguang;

/**
  Controller 控制层     客户端的请求处理与响应
    调用service
  Service  业务层
    业务处理
    登录逻辑校验   分页的处理   查询商品列表(页码转换)
    调用Dao
  Dao  持久层
     执行数据库语句
    跟数据库打交道

  Service 业务逻辑操作
  业务层
  接口的作用
  统一一种规范
  其他开发人员方便查看，而不需要关心具体的实现

    事务
 */
public class UserServiceImpl implements UserService {
    @Override
    public void add() {
//        System.out.println("开启事务");
        System.out.println("新增一个User");
//        System.out.println("提交事务");
    }



    @Override
    public void delete() {
//        System.out.println("开启事务");
        System.out.println("删除一个User");
//        System.out.println("提交事务");
    }

    @Override
    public void select() {
//        System.out.println("开启事务");
        System.out.println("查询一个User");
//        System.out.println("提交事务");
    }

    @Override
    public void update() {
//        System.out.println("开启事务");
        System.out.println("修改一个User");
//        System.out.println("提交事务");
    }
}
