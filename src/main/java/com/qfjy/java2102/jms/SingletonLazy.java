package com.qfjy.java2102.jms;

class SingletonLazy {
    /**
     * 单例模式---懒汉式
     */

    private static SingletonLazy s;

    private SingletonLazy() {
    }

    /**
     * 解决并发线程不安全问题
     */
    public synchronized static SingletonLazy getInstance() {
        if (null == s)
            s = new SingletonLazy();
        return s;
    }
}