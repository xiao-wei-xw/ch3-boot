package com.qfjy.java2102.zhangjunfei;

/**
 * @author Torso
 * @create 2021-08-04 16:48
 */
//静态代理类
public class StaticProxyWork implements Work{
    private Work mywork;
    public StaticProxyWork(Work mywork) {
        this.mywork = mywork;
    }
    //代理重构work方法
    @Override
    public void work() {
        System.out.println("开始工作前的准备");
        mywork.work();
        System.out.println("工作结束后的操作");
    }
}
