package com.qfjy.java2102.zhangjunfei;

/**
 * @author Torso
 * @create 2021-08-04 16:47
 */
//工作接口的实现类
public class DoWork implements Work{
    //重写接口中的工作方法
    @Override
    public void work() {
        System.out.println("认真完成工作");
    }
}
