package com.qfjy.java2102.zhangjunfei;

/**
 * @author Torso
 * @create 2021-08-04 16:54
 */
//测试静态代理
public class StaticProxyTest {
    public static void main(String[] args) {
        //创建工作接口的实现类对象
        DoWork work = new DoWork();
        //创建静态代理类对象，并将实现来对象作为参数传到静态代理类中
        StaticProxyWork mywork = new StaticProxyWork(work);
        //调用静态代理类中对代理实现类重构后的work方法
        mywork.work();
    }
}
