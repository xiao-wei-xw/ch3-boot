package com.qfjy.java2102.wangyan;

/**
 * DATE: 2021/8/4
 * Author: (王炎)
 */

/**
 * 代理模式的分享
 * 简介:代理模式的功能就是代理访问对象，由于一些原因，对象不能够直接被访问引用
 * 优点：1.对目标对象具有一定的保护作用
 *      2.可以扩展目标对象的功能
 *      3.将客户端与目标对象分离，一定程度上降低了程序的耦合性
 * 缺点：1.会使程序中的类增加
 *      2.请求处理速度会变慢（JDBC访问速率为什么比mybatis快的原因）
 *      3.增加了系统的复杂度
 */
public class ProxyTest {
    public static void main(String[] args) {
        Proxy proxy = new Proxy();
        proxy.test();
    }
}

/**
 * 目标对象接口
 */
interface Target{
    /**
     * 这是目标对象的test方法
     */
    void test();
}

/**
 * 实现目标对象接口
 */
class TargetImpl implements  Target{

    @Override
    public void test() {
        System.out.println("这是目标对象的test方法");
    }
}

/**
 * 这是代理类
 */
class Proxy implements Target{
    private TargetImpl target;
    @Override
    public void test() {
        if (target==null){
            target=new TargetImpl();
        }
        before();
        target.test();
        after();
    }

    public void before(){
        System.out.println("访问目标对象之前进行操作");
    }

    public void after(){
        System.out.println("访问目标对象之后进行操作");
    }
}