package com.qfjy.java2102.zhanglei;

public class ProxyTest {
    public static void main(String[] args) {



        Name name = new ProxyName("张三");
        name.show();

        /**
         * 结果：
         * RealName的loadName:张三
         * RealName的show: 张三
         *
         * 通过ProxyName来实现RealName的对象的创建和方法调用
         */


    }
}
