package com.qfjy.java2102.zhanglei;

public class RealName implements Name {

    //真正的姓名
   private String realName;

    public RealName(String realName) {
        this.realName = realName;
        loadName(realName);
    }

    /**
     * 继承接口的方法
     */
    @Override
    public void show() {
        System.out.println("RealName的show: "+realName);
    }

    /**
     * 加载名字方法
     * @param realName
     */
    private void loadName(String realName){
        System.out.println("RealName的loadName:"+realName);
    }

}
