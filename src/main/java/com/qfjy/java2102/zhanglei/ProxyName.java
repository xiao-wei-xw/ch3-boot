package com.qfjy.java2102.zhanglei;

public class ProxyName implements Name {


    //真实姓名
    private RealName realName;
    //代理姓名
    private  String proxyName;

    public ProxyName(String proxyName) {
        this.proxyName = proxyName;
    }

    @Override
    public void show() {
        if (realName==null){
            realName = new RealName(proxyName);
        }
        realName.show();
    }
}
