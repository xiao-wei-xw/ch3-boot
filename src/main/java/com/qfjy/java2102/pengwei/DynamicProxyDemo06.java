package com.qfjy.java2102.pengwei;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class DynamicProxyDemo06 implements InvocationHandler{
    private Object target;

    public DynamicProxyDemo06(Object target){
        this.target = target;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        operator1();
        Object result = method.invoke(target,args);
        operator2();

        return result;
    }

    public void operator1(){
        System.out.println("添加的操作1");
    }

    public void operator2(){
        System.out.println("添加的操作2");
    }
}
