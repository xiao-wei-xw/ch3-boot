package com.qfjy.java2102.pengwei;
import java.lang.reflect.Proxy;

public class DynamicProxyTestDemo06 {
    public static void main(String[] args) {
        Hello h = new HelloImp();
        DynamicProxyDemo06 dy = new DynamicProxyDemo06(h);
        Hello hello = (Hello)Proxy.newProxyInstance(h.getClass().getClassLoader(),
                h.getClass().getInterfaces(),
                dy);

        hello.say("Sherlock");
    }
}